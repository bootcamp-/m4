import csv

class Vehiculo():
    def __init__(self, marca, modelo, numero_ruedas):
        self.marca = marca
        self.modelo = modelo
        self.numero_ruedas = numero_ruedas

    def guardar_datos_csv(self):
        try:
            with open("vehiculos.csv", "a", newline="") as f:
                datos = [(self.__class__, self.__dict__)]
                archivo_csv = csv.writer(f)
                archivo_csv.writerows(datos)
        except FileNotFoundError:
            print("El archivo solicitado no existe")
        except Exception as e:
            print(f"Error: {e}")

    def leer_datos_csv(self):
        try:
            with open("vehiculos.csv", "r") as f:
                archivo_csv = csv.reader(f)
                print(f"Lista de Vehículos {type(self).__name__}")
                for i in archivo_csv:
                    tipo = str(self.__class__)
                    if tipo in i[0]:
                        print(i[1])
        except FileNotFoundError:
            print("El archivo solicitado no existe")
        except Exception as e:
            print(f"Error: {e}")


    def __str__(self):
        return f"Marca {self.marca}, Modelo {self.modelo}, {self.numero_ruedas} ruedas"
    
class Automovil(Vehiculo):
    def __init__(self, marca, modelo, numero_ruedas, velocidad, cilindrada):
        super().__init__(marca, modelo, numero_ruedas)
        self.velocidad = velocidad
        self.cilindrada = cilindrada

    def __str__(self):
        return super().__str__() + f", {self.velocidad} km/h, {self.cilindrada} cc"
    
class Particular(Automovil):
    def __init__(self, marca, modelo, numero_ruedas, velocidad, cilindrada, numero_puestos):
        super().__init__(marca, modelo, numero_ruedas, velocidad, cilindrada)
        self.numero_puestos = numero_puestos

    def get_numero_puestos(self):
        return self.numero_puestos
    
    def set_numero_puestos(self, nuevo_numero_puestos):
        self.numero_puestos = nuevo_numero_puestos

    def __str__(self):
        return super().__str__() + f", {self.numero_puestos} puestos"
        
class Carga(Automovil):
    def __init__(self, marca, modelo, numero_ruedas, velocidad, cilindrada, carga):
        super().__init__(marca, modelo, numero_ruedas, velocidad, cilindrada)
        self.carga = carga
    
    def get_carga(self):
        return self.carga
    
    def set_carga(self, nueva_carga):
        self.carga = nueva_carga

    def __str__(self):
        return super().__str__() + f", carga de {self.carga} kg"
    
class Bicicleta(Vehiculo):
    def __init__(self, marca, modelo, numero_ruedas, tipo):
        super().__init__(marca, modelo, numero_ruedas)
        self.tipo = tipo

    def __str__(self):
        return super().__str__() + f", tipo {self.tipo}"
    
class Motocicleta(Bicicleta):
    def __init__(self, marca, modelo, numero_ruedas, tipo, motor, cuadro, numero_radios):
        super().__init__(marca, modelo, numero_ruedas, tipo)
        self.motor = motor
        self.cuadro = cuadro
        self.numero_radios = numero_radios

    def __str__(self):
        return super().__str__() + f", {self.numero_radios} radios, cuadro {self.cuadro}, motor {self.motor}"