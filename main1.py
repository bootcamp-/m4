from classes import *

instancias = []

n = int(input("Cuantos vehículos desea insertar: "))

for i in range (n):
    print(f"Datos del automóvil {i+1}")
    marca = input("Inserte la marca del automóvil: ")
    modelo = input("Inserte el modelo: ")
    numero_ruedas = int(input("Inserte el número de ruedas: "))
    velocidad = int(input("Inserte la velocidad en km/h: "))
    cilindrada = int(input("Inserte la cilindrada en cc: "))
    print("-"*80)
    auto = Automovil(marca, modelo, numero_ruedas, velocidad, cilindrada)
    instancias.append(auto)

print("Imprimiendo por pantalla los vehículos:")

for index,item in enumerate(instancias):
    print(f"Datos del automóvil {index+1}: Marca {item.marca}, Modelo {item.modelo}, {item.numero_ruedas} ruedas, {item.velocidad} km/h, {item.cilindrada} cc")